package com.example.stylinglayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.stylinglayouts.databinding.ActivityBottomSheetBinding



//ITS NOT USED IN this PROJECT

class BottomSheetActivity : AppCompatActivity() {
    lateinit var binding: ActivityBottomSheetBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBottomSheetBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetTransparentCornerTheme)

    }

    /*private fun setStyle(styleNormal: Int, customBottomSheetTransparentCornerTheme: Int) {
       setContentView(customBottomSheetTransparentCornerTheme)
    }*/

    /*override fun getTheme(): Int = R.style.CustomBottomSheetTransparentCornerTheme
    companion object {
        val TAG = BottomSheetActivity::class.simpleName
        fun newInstance() = BottomSheetActivity()
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, ): View
    {
        binding = ActivityBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }
}*/
}