package com.example.stylinglayouts

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.drawable.TransitionDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.stylinglayouts.databinding.ActivityCrossfadeBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class CrossfadeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCrossfadeBinding

    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCrossfadeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val media=ContextCompat.getDrawable(this,R.drawable.headerpic)
        Glide.with(this.binding.roundImg).load(media).apply(RequestOptions.circleCropTransform()).into(binding.roundImg)

        val mySharedprefrnc=getSharedPreferences("BtmsheetValues", MODE_PRIVATE)
        val name= mySharedprefrnc.getString("name","User Name")
        val no=mySharedprefrnc.getString("number","User Mobile")
        val place=mySharedprefrnc.getString("place","User Address")
        binding.userName.setText(name)
        binding.userMobile.setText(no)
        binding.userAddress.setText(place)


    }

}

