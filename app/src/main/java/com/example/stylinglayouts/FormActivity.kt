package com.example.stylinglayouts

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.stylinglayouts.databinding.ActivityFormBinding
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FormActivity : AppCompatActivity() {
    var c : Calendar = Calendar.getInstance()
    private var df : SimpleDateFormat? = null
    private var formattedDate = ""
    private lateinit var binding: ActivityFormBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityFormBinding.inflate(layoutInflater)
        setContentView(binding.root)
        df = SimpleDateFormat("dd-MM-yyyy HH:mm a")
        formattedDate = df!!.format(c.time)
        println("Format dateTime => $formattedDate")
        //val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")

       // val today = Date()

        //val todayWithZeroTime: Date = formatter.parse(formatter.format(today))
       // Toast.makeText(this, "${todayWithZeroTime}", Toast.LENGTH_SHORT).show()
        Log.d("x", "onCreate: ${formattedDate}")
        val my_autoTextView=binding.AutocompleteText
        val countries= arrayListOf<String>("India","Albania","Argentina","Brazil","Australia","Pakistan","Italy","England")
        val adapter= ArrayAdapter(this,android.R.layout.simple_list_item_1,countries)
        my_autoTextView.setAdapter(adapter)

        binding.btnSubmit.setOnClickListener(){
            val current_Text= my_autoTextView.text
            Toast.makeText( this,current_Text, Toast.LENGTH_SHORT).show()
        }

        binding.btnMakeEditText.setOnClickListener(){
            addEditText()
        }

            binding.cvCalendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
                val selectedDate:String="Selected - "+dayOfMonth+"/"+(month+1)+"/"+year
                Toast.makeText(this,selectedDate,Toast.LENGTH_SHORT).show()
                Log.d("dated", "newwwwww${Calendar.DATE}${Calendar.getInstance()} ")

            }


    }

    private fun addEditText() {

            // Create EditText
            val editText = EditText(this)
            editText.setHint(R.string.enter_something)
            editText.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            editText.setPadding(20, 20, 20, 20)


            // Add EditText to LinearLayout
            binding.root.addView(editText)
    }
}