package com.example.stylinglayouts

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stylinglayouts.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityHomeBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupViewPager2()


        var tilelist = mutableListOf(
            TileModel(
                1, R.drawable.headerpic, "Assignment", "Physics work"
            ),
            TileModel(2, R.drawable.book1, "Seminar Discussion", "Conducting gd"),
            TileModel(3, R.drawable.book2, "Onam Examination", "Examination"),
            TileModel(4, R.drawable.book3, "Assessment", "Internal assessment"),
            TileModel(5, R.drawable.book5, "Arts Celebration", "Entertainment"),

            )
        binding.rvHome.layoutManager = LinearLayoutManager(this)

        val new_adapter = TileAdapter(this.applicationContext, tilelist)
        binding.rvHome.adapter = new_adapter


    }
    private fun setupViewPager2() {
        val list: MutableList<Int> = ArrayList()
        list.add(R.drawable.book1)
        list.add(R.drawable.book2)
        list.add(R.drawable.book3)


        // Set adapter to viewPager.
        binding.viewPager.adapter = PagerAdapter(this, list)
    }

}