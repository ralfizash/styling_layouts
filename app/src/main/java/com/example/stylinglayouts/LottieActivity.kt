package com.example.stylinglayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.stylinglayouts.my_gpay.GpayAftersendActivity

class LottieActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lottie)
        Handler().apply {
            val runnable = object : Runnable {
                override fun run() {
                    val i=Intent(applicationContext,GpayAftersendActivity::class.java)
                    startActivity(i)
                }
            }
            postDelayed(runnable, 3000)
        }
    }

}