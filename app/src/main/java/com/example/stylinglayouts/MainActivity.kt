package com.example.stylinglayouts

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.stylinglayouts.databinding.ActivityHomeBinding
import com.example.stylinglayouts.databinding.ActivityMainBinding
import com.example.stylinglayouts.my_gpay.ActivityGpaySendMoney
import com.google.android.material.bottomsheet.BottomSheetDialog

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        binding=ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        var btnalpha:Int=0

        binding.apply {
            btnLottie.setOnClickListener(){
                val pageLottie=Intent(this@MainActivity,LottieActivity::class.java)
                startActivity(pageLottie)
            }
            btnOverlap.setOnClickListener(){
                val pageImageFilter=Intent(this@MainActivity,ImagefilterviewActivity::class.java)
                startActivity(pageImageFilter)
            }
            btnMockview.setOnClickListener(){
                val pagemockView=Intent(this@MainActivity,MockviewActivity::class.java)
                startActivity(pagemockView)

            }

            btnProfilecard.setOnClickListener(){
                val crossfade=Intent(this@MainActivity,CrossfadeActivity::class.java)
                startActivity(crossfade)
            }
            btnAlpha.setOnClickListener(){

                if (btnalpha==0) {
                    binding.tvSubtitle.alpha = 1f
                    btnalpha=1
                }
                else
                {
                    binding.tvSubtitle.alpha=0f
                    btnalpha=0
                }

            }
            btnDynamic.setOnClickListener(){
                val dynamic_editText=Intent(this@MainActivity,FormActivity::class.java)
                startActivity(dynamic_editText)
            }

            //profile form
            btnForm.setOnClickListener(){
                val profileformView=Intent(this@MainActivity, ProfileActivity::class.java)
                startActivity(profileformView)
            }

            btnGpay.setOnClickListener(){
                val gpay = Intent(this@MainActivity, ActivityGpaySendMoney::class.java)
                startActivity(gpay)

            }

            btnHome.setOnClickListener(){
                val home = Intent(this@MainActivity, HomeActivity::class.java)
                startActivity(home)

            }

            btnToggle.setOnCheckedChangeListener { buttonView, isChecked ->
                val msg = "Toggle Button is " + if (isChecked) "ON" else "OFF"
                Toast.makeText(this@MainActivity, msg, Toast.LENGTH_SHORT).show()
            }

            btnQrcode.setOnClickListener(){
                val qrpage = Intent(this@MainActivity, TestActivity::class.java)
                startActivity(qrpage)
            }

            btnSocial.setOnClickListener(){
                val socialPage = Intent(this@MainActivity, SocialActivity::class.java)
                startActivity(socialPage)
            }

            btnTest.setOnClickListener(){
                val btmSheet = BottomSheetDialog(this@MainActivity,R.style.SheetDialog)

                //setContentView(R.drawable.gradient_curve)
                val view= layoutInflater.inflate(R.layout.mybottom_sheet,null)
                //view.setBackgroundResource(R.drawable.gradient_curve)

                val btnUpdate = view.findViewById<Button>(R.id.btnUpdate)
                val new_name = view.findViewById<EditText>(R.id.etUpdateName)
                val new_no= view.findViewById<EditText>(R.id.etUpdateNumber)
                val new_job = view.findViewById<EditText>(R.id.etUpdateJob)
                val new_place = view.findViewById<EditText>(R.id.etUpdatePlace)
                val new_qualif = view.findViewById<EditText>(R.id.etUpdateQualif)

                btnUpdate.setOnClickListener(){
                val mySharedprefrnc=getSharedPreferences("BtmsheetValues", MODE_PRIVATE)
                    val editor:SharedPreferences.Editor=mySharedprefrnc.edit()
                    editor.putString("name",new_name.text.toString())
                    editor.putString("number",new_no.text.toString())
                    editor.putString("place",new_place.text.toString())
                    editor.commit()
                    btmSheet.dismiss()

                }
                btmSheet.setCancelable(true)
                btmSheet.setContentView(view)
                btmSheet.show()


               /* val bottomSheet = Intent(this@MainActivity,BottomSheetActivity::class.java)
                startActivity(bottomSheet)*/
            }
            btnTab.setOnClickListener(){
                val TabPage = Intent(this@MainActivity, TabActivity::class.java)
                startActivity(TabPage)
            }




        }
    }
    /*override fun setContentView(layoutResId : Int){
        l
    }*/
}