package com.example.stylinglayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MockviewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mockview)
    }
}