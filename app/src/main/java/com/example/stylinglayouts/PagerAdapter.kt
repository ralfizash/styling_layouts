package com.example.stylinglayouts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stylinglayouts.databinding.ItemholderBinding

import org.json.JSONObject




class PagerAdapter(
    private val context: Context,
    private val imglist: MutableList<Int>

) : RecyclerView.Adapter<PagerAdapter.ViewPagerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerHolder {
        val binding = ItemholderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewPagerHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewPagerHolder, position: Int) {
       // val label = labelList[position]
       // val color = colorList[position]

        holder.bind(imglist[position])
    }

    override fun getItemCount(): Int {
        return imglist.size
    }

   inner class ViewPagerHolder(private var itemHolderBinding: ItemholderBinding) :
        RecyclerView.ViewHolder(itemHolderBinding.root) {
        fun bind(img: Int) {
            val media=img
            itemHolderBinding.mainImage.setImageResource(media)
        }

        }
    }
