package com.example.stylinglayouts

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.example.stylinglayouts.databinding.ActivityProfileFormBinding
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

class ProfileActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {
    private val pickImage = 100
    private var imageUri: Uri? = null
    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0
    public lateinit var dob: String
    public lateinit var time: String
    public lateinit var msg: String
    lateinit var radioButton: RadioButton
    var cal = Calendar.getInstance()
    private lateinit var binding: ActivityProfileFormBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        binding = ActivityProfileFormBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupSpinner()
        setCheckedChangeListener()


        binding.datePicker.setOnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
            val selectedDate: String = "${dayOfMonth}/${(monthOfYear + 1)}/${year}"
            //Toast.makeText(this,selectedDate,Toast.LENGTH_SHORT).show()
            Log.d("date", "onCreate: ${selectedDate}")
        }

        binding.btnTime.setOnClickListener() {
            val timePickerDialog = TimePickerDialog(
                this@ProfileActivity, this@ProfileActivity, hour, minute,
                DateFormat.is24HourFormat(this)
            )
            timePickerDialog.show()
        }

        binding.radioResultCheck.setOnClickListener() {
            val selectedOption: Int = binding.rGroup!!.checkedRadioButtonId

            // Assigning id of the checked radio button
            radioButton = findViewById(selectedOption)
            val radioItemSelected = radioButton.text

            // Displaying text of the checked radio button in the form of toast
            Toast.makeText(this, radioButton.text, Toast.LENGTH_SHORT).show()
        }

        //CheckBox
        binding.btnCheckboxResult.setOnClickListener {

            val result = StringBuilder()
            result.append("Selected Items")
            if (binding.checkBox1.isChecked) {
                result.append("\nMusic")

            }
            if (binding.checkBox2.isChecked) {
                result.append("-Sports")

                if (binding.checkBox3.isChecked) {
                    result.append("-Literature")

                }
                if (binding.checkBox4.isChecked) {
                    result.append("-Travel")

                }
                Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_SHORT).show()
            }


        }

        binding.buttonLoadPicture.setOnClickListener {
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, pickImage)
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        myDay = day
        myYear = year
        myMonth = month
        val calendar: Calendar = Calendar.getInstance()
        hour = calendar.get(Calendar.HOUR)
        minute = calendar.get(Calendar.MINUTE)
        Toast.makeText(this, "${myDay}/${myMonth}/${myYear}", Toast.LENGTH_SHORT).show()


        /*val timePickerDialog = TimePickerDialog(this@ProfileActivity, this@ProfileActivity, hour, minute,
            DateFormat.is24HourFormat(this))
        timePickerDialog.show()*/
    }


    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        time = "Selected time is ${hourOfDay}:${minute}"
        Log.d("time", "onTimeSet: hour-${hourOfDay}-Minute ${minute}")
        binding.btnTime.setText(time)
    }

    //picture Upload
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == pickImage) {
            imageUri = data?.data
            binding.ivProfile.setImageURI(imageUri)
        }

    }

    private fun setupSpinner() {
        val personNames = arrayOf("Rahul", "Jack", "Rajeev", "Aryan", "Rashmi", "Jaspreet", "Akbar")
        val spinner = binding.Spinner
        val arrayAdapter = ArrayAdapter(this, R.layout.spinneritem, personNames)
        spinner.adapter = arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                Toast.makeText(
                    this@ProfileActivity,
                    " Selected Value- ${personNames[position]}",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

    }

    //Switch function
    @SuppressLint("ResourceAsColor")
    private fun setCheckedChangeListener() {
        binding.switchID.setOnCheckedChangeListener { _, isChecked ->
            val msg = (if (isChecked) "ON" else "Off")
            Toast.makeText(this@ProfileActivity, msg, Toast.LENGTH_SHORT).show()
            if (isChecked){
                binding.clContent.setBackgroundColor(R.color.black)
            }
            else
            {
                binding.clContent.setBackgroundColor(R.color.white)
            }
            binding.switchID.text = msg
        }
    }

}