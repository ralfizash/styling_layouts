package com.example.stylinglayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.stylinglayouts.my_gpay.GpayAftersendActivity

class SendedLotie : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sended_lotie)
        Handler().apply {
            val runnable = object : Runnable {
                override fun run() {
                    val i=Intent(applicationContext,SocialActivity::class.java)
                    startActivity(i)
                }
            }
            postDelayed(runnable, 3000)
        }
    }
}