package com.example.stylinglayouts

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class SocialActivity : AppCompatActivity() {
    private val pictureCode = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social)

        val btnShareImage = findViewById<Button>(R.id.shareImage)
        btnShareImage?.setOnClickListener {
            selectImage() }


        val btnShare = findViewById<Button>(R.id.shareIt)
        btnShare?.setOnClickListener {
            val editText = findViewById<EditText>(R.id.shareText)
            if (editText != null) {
                val text = editText.text.toString()
                if (!text.isEmpty()) {
                    startShareText(text)
                } else {
                    Toast.makeText(applicationContext, "Please enter some text", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
//select image
    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), pictureCode)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && resultCode == Activity.RESULT_OK && (requestCode == pictureCode)) {
            startShareImage("A picture for you..", data.data)
        }

    }
    private fun startShareImage(text: String, uri: Uri?) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(Intent.createChooser(sharingIntent, "Perfect"))
    }


//send message
    private fun startShareText(text: String?) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, text)
        sendIntent.type = "text/plain"
        //startActivity(sendIntent)

        // Set package only if you do not want to show all the options by which you can share the text.
        // Setting package bypass the system picker and directly share the data on WhatsApp.
        // TODO uncomment code to show whatsapp directly
         //sendIntent.setPackage("com.whatsapp");

        startActivity(sendIntent)


    }

   /* override fun onRestart() {
        super.onRestart()
        val lotiesend=Intent(this,SendedLotie::class.java)
        startActivity(lotiesend)
    }*/

}