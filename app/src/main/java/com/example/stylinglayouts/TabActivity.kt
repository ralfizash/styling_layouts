package com.example.stylinglayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stylinglayouts.databinding.ActivityTabBinding

class TabActivity : AppCompatActivity() {
    private lateinit var binding:ActivityTabBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding=ActivityTabBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val list= mutableListOf(
            TabModel("Vishnu"),
            TabModel("Anandh Bose"),
            TabModel("Zenith"),
            TabModel("Ashiq"),
            TabModel("Nithin"),
            TabModel("Leo"),
            TabModel("Anil"),
            TabModel("Bimal")
        )
        binding.myRv.layoutManager= LinearLayoutManager(this)
        val myadapter=TabAdapter(this,list)
        binding.myRv.adapter=myadapter


    }
}