package com.example.stylinglayouts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stylinglayouts.databinding.TabItemBinding

class TabAdapter(
    private val context: Context,
    var list: List<TabModel>):RecyclerView.Adapter<TabAdapter.TabViewHolder>() {


    inner class TabViewHolder(private val binding:TabItemBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(item:TabModel){
            binding.apply {
                tvTabname.text=item.name
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabViewHolder {
        val binding=TabItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return TabViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TabViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}