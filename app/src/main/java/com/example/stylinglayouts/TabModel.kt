package com.example.stylinglayouts

import java.io.Serializable

data class TabModel(
    val name:String)
    :Serializable
