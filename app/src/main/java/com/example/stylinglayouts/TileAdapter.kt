package com.example.stylinglayouts

import android.content.Context
import android.graphics.Shader
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.stylinglayouts.databinding.CardItemBinding

class TileAdapter( private val context: Context,
                    private var Tilelist: List<TileModel>):
    RecyclerView.Adapter<TileAdapter.TileviewHolder>() {

  val click: ((TileModel) ->Unit)?=null
//val msg = binding.ratingBar.rating.toString()
//            Toast.makeText(this@MainActivity, msg, Toast.LENGTH_SHORT).show()

    inner class TileviewHolder(private val binding: CardItemBinding): RecyclerView.ViewHolder(binding.root)
    {
   fun bind(item:TileModel){
       binding.apply {
           ctvTitle.text=item.title
           ctvSubtitle.text=item.subtitle
           val media = item.image
           Glide.with(context).load(media).into(civMain)

           ivVerified.setOnClickListener(){
               val msg = binding.rBar.rating.toString()
              Toast.makeText(context,"Rating -  ${msg}", Toast.LENGTH_SHORT).show()
                //click?.invoke()
           }
       }
   }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TileviewHolder {
        val binding=CardItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return TileviewHolder(binding)
    }

    override fun onBindViewHolder(holder: TileviewHolder, position: Int) {
        holder.bind(Tilelist[position])

    }

    override fun getItemCount(): Int {
        return Tilelist.size
    }
}