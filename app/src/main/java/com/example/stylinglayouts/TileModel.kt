package com.example.stylinglayouts

import android.graphics.drawable.Drawable
import java.io.Serializable


data class TileModel(
    var id:Int,
    var image: Int,
    var title: String,
    var subtitle: String

): Serializable