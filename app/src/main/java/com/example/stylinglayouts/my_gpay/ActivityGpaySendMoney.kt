package com.example.stylinglayouts.my_gpay

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.stylinglayouts.LottieActivity
import com.example.stylinglayouts.R
import com.example.stylinglayouts.databinding.ActivityGpaySendmoneyBinding
import com.google.android.material.snackbar.Snackbar


class ActivityGpaySendMoney : AppCompatActivity() {
    private lateinit var binding: ActivityGpaySendmoneyBinding
    override fun onCreate(savedInstanceState: Bundle?) {

        binding= ActivityGpaySendmoneyBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.etMoney.requestFocus()

        /*val bundle=Bundle()
        bundle.putString("Amount",binding.etMoney.text.toString())
        bundle.putString("Note",binding.etAddNote.text.toString())*/


        binding.FabIcon.setOnClickListener(){
            if (binding.etMoney.text.toString().isBlank()){
                val snackBar = Snackbar.make(
                    it, "Payement must be atleast ₹1",
                    Snackbar.LENGTH_LONG
                ).setAction("Action", null)
                snackBar.setActionTextColor(Color.BLACK)
                val snackBarView = snackBar.view
                snackBarView.setBackgroundColor(Color.parseColor("#f2eeed"))
                val textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.BLACK)
                snackBar.show()
                //Toast.makeText(applicationContext, "Payement must be atleast ₹1", Toast.LENGTH_LONG).show()
            }
            else
            {
                val sp=getSharedPreferences("UserValues",Context.MODE_PRIVATE)
                val ed:SharedPreferences.Editor=sp.edit()
                ed.putString("Amount",binding.etMoney.text.toString())
                ed.putString("Note",binding.etAddNote.text.toString())
                ed.commit()
                val lotie=Intent(this@ActivityGpaySendMoney,LottieActivity::class.java)

                startActivity(lotie)
            }

        }
    }
}