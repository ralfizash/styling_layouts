package com.example.stylinglayouts.my_gpay

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.stylinglayouts.R
import com.example.stylinglayouts.databinding.ActivityGpayAftersendBinding
import com.example.stylinglayouts.databinding.ActivityGpaySendmoneyBinding
import java.time.LocalDateTime

class GpayAftersendActivity : AppCompatActivity() {
    private lateinit var binding:ActivityGpayAftersendBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        binding=ActivityGpayAftersendBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val sp=getSharedPreferences("UserValues", Context.MODE_PRIVATE)
        val amt=sp.getString("Amount","Error")

        Log.d("cash", "onCreate: ${amt}")

        binding.tvBodyamount.setText(sp.getString("Amount","Error"))
        binding.tvBodyaddnote.setText(sp.getString("Note","No message"))

        val current = LocalDateTime.now()
        val date=current.dayOfMonth
        val month=current.month
        val x="Completed on ${month} ${date}"
        binding.tvTime.text=x
        binding.clPay.setOnClickListener(){
            val sendIntent=Intent(this,ActivityGpaySendMoney::class.java)
            startActivity(sendIntent)
        }


    }
}